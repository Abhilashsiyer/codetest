import { verifyAllLinks, visit, verifyCauseBanner } from "../utils/globalUtils"
import links from "../fixtures/getLinks.json" 

describe('Verify Existing data', () => {
  
  it('Verify pre-existing links are shown appropriately', () => {
    visit('/swe_test');
    verifyAllLinks({items:links.data.getLinks.items});// Verifying existing links from the saved json ../fixtures/getLinks.json
  })

  it('Verify cause banner content', () => {
    visit('/swe_test');
    verifyCauseBanner({
      title:'Support Anti-Racism',
      paragragh:'I’m raising awareness, driving donations and sharing information in support of justice and equality.',
      link:'https://linktr.ee/action'});
  })

})