import { addNewLink, deleteLink, login, verifyLinkAdded, verifyUnsafeURL, visit } from "../utils/globalUtils"

describe('Sanity code tests', () => {
  it('Verify if a link can be added successfully', () => {
    visit('/login');
    login({
      username: 'swe_test',
      password: 'Engineer123!',
    });
    addNewLink({
      title: 'Test',
      url: 'https://wwww.test.com',
    });
    visit('/swe_test');
    verifyLinkAdded({
      title: 'Test',
      url:'https://wwww.test.com'
  });
    visit('/admin');
    deleteLink();
  })

  it('Verify if unsafeURL error is shown for unsafe URL', () => {
    visit('/login');
    login({
      username: 'swe_test',
      password: 'Engineer123!',
    });
    addNewLink({
      title: 'Unsafe URL Test',
      url: 'http://test',
    });
    verifyUnsafeURL();
    deleteLink();    
  })
})