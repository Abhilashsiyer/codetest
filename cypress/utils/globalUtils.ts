/// <reference types="cypress" />
/// <reference types="cypress-iframe" />
import 'cypress-xpath';
export const visit = (url: string) => {
    cy.visit(url);
    Cypress.on('uncaught:exception', () => {
      // returning false here prevents Cypress from
      // failing the test
      return false;
    });
  };

  export const login = ({
    username,
    password
  }: {
    username: string;
    password: string;
  }) => {
    cy.get('[name=username]').type(username);
    cy.get('[name=password]').type(password);
    cy.get('[data-test=Button]').click();
  };

  export const addNewLink = ({
    title,
    url
  }: {
    title: string;
    url: string;
  }) => {
    cy.get('[data-testid=LinkEditor_Link_Add_Button]').click();
    cy.wait(6000); // There is a delay in loading of Link. Need to look for other wait options
    cy.get('[data-testid=title_Input_V1Label]').first().click();
    cy.xpath('//input[@name="title"]').first().type(title);
    cy.get('[data-testid=url_Input_V1Label]').first().click();
    cy.xpath('//input[@name="url"]').first().type(url);
    saveLink();
  };

  export const saveLink = () => {
    cy.get('[data-test=IconComponent]').first().click();
    cy.get('[data-test=IconComponent]').first().click();
  };

  export const deleteLink = () => {
    cy.get('[data-testid=LinkEditor_DeletePanel_Toggle_Button]').first().click();
    cy.get('[data-testid=LinkEditor_DeletePanel_Delete_Button]').first().click();
  };

  export const verifyLink = () => {
    cy.get('[data-testid=LinkEditor_DeletePanel_Toggle_Button]').first().click();
    cy.get('[data-testid=LinkEditor_DeletePanel_Delete_Button]').first().click();
  };

  export const verifyUnsafeURL = () => {
    // Need to add data test id to the p tag where Unsafe URL text is shown. Using xpath for time being
    cy.xpath("//*[contains(text(),'Unsafe URL')]").should('be.visible');
  };

  export const verifyLinkAdded = ({
    title,
    url
  }: {
    title: string;
    url:string;

  }) => {
    cy.get('[data-testid=LinkButton]').should('be.visible').contains(title);
    cy.get('[data-testid=LinkButton]').should('have.attr', 'href').and('include', url);
  };

  export const verifyAllLinks = ({
    items,
  }: {
    items: any;

  }) => {
    Cypress._.range(items.length).forEach((index: number) => {
      //Unique identifiers to be added in the application to recognise every link. For example data-testid="LinkButton-Title"
      cy.get('[data-testid=LinkButton]').should('be.visible').contains(items[index].title);
      if (items[index].type.includes('CLASSIC')){
        cy.get('[data-testid=LinkButton]').should('have.attr', 'href').and('include', items[index].url);
      }

      if (items[index].type.includes('YOUTUBE_VIDEO')){
        cy.get('[data-testid=LinkEmbedButton]').first().click();
        const orginalUrlId = items[index].url.split('=')[1];
        const embedYoutubeURL = `https://www.youtube.com/embed/${orginalUrlId}`
        cy.log(embedYoutubeURL);

        cy.get('[data-testid=StyledContainer]')
        .should('be.visible')
        .within(() => {
        cy.get('iframe').should('have.attr', 'src').and('contain', embedYoutubeURL)
      });

      }
  
  });
      
  };  

  export const verifyCauseBanner = ({
    title,
    paragragh,
    link
  }: {
    title: string;
    paragragh: string;
    link:string;

  }) => {
    cy.get(`[data-testid=CauseBannerContentOpen]`)
    .should('be.visible')
    .within(() => {
      cy.get('h6').should('be.visible').contains(title);
  });

  cy.get(`[data-testid=CauseBannerContentOpen]`)
    .should('be.visible')
    .within(() => {
      cy.get('p').should('be.visible').contains(paragragh);
  });

  cy.get(`[data-testid=CauseBannerContentOpen]`)
    .should('be.visible')
    .within(() => {
      cy.get('a').should('have.attr', 'href').and('include', link)
  });
};