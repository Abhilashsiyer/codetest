# codeTest

Tech Test challenge completed using cypress

To run the test -

1. Do npm install
2. Do npx cypress open --config-file cypress.json from base of this project
3. Select **sampleCheck** to run two tests -

```
Test 1- Adds, verifies and deletes a Link
Test 2- Adds, verifies and deletes a Link
```

4. Go back to all tests and now select **verifyExcistingData**. This will also run two tests -

```
Test 1- Verifies preexsting LinkTree blog and Youtube Links
Test 2- Verifies BLM custom banner
```

